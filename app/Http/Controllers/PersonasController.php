<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Persona;

class PersonasController extends Controller
{
    public function index(){
        return view('index');
    }
    public function nuevaPersona(){
        return view('personas.nuevaPersona');
    }
    public function tablaPersonas(){
        return view('personas.tabla',['posts'=>Persona::orderBy('id')->paginate(5)]);
    }
    public function store(Request $request){

        $request->validate([
            'dni' => 'required|unique:personas|max:8',
            'nombre' => 'required',
            'apellido' => 'required',
            'fecha' => 'required',
        ]);


        $persona = new Persona;
        $persona->nombre=$request['nombre'];
        $persona->apellido=$request['apellido'];
        $persona->dni=$request['dni'];
        $persona->fecha_nacimiento=$request['fecha'];
        $persona->save();
        return redirect()->route('tablaPersonas',['posts'=>Persona::orderBy('id')->paginate(5)]);
    }
    public function destroy(Persona $persona){
        $persona->delete();
        return back();
    }
    public function edit(Persona $persona){
        return view('personas.editarPersona',['persona'=>$persona]);
    } 

    public function update(Persona $persona, Request $request){
        $persona->nombre=$request->nombre;
        $persona->apellido=$request->apellido;
        $persona->dni=$request->dni;
        $persona->fecha_nacimiento=$request->fecha;
        $persona->save();
        return redirect()->route('tablaPersonas',['posts'=>Persona::orderBy('id')->paginate(5)]);
    } 
     
}
