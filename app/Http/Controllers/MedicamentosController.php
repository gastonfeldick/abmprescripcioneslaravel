<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medicamento;

class MedicamentosController extends Controller
{
    public function create(){
        return view('medicamentos.createMedicamento');
    }

    public function index(){

        return view('medicamentos.tableMedicamentos',['posts'=>Medicamento::orderBy('id')->paginate(5)]);
    }

    public function store(Request $request){
        
        $request->validate([
            'nombre_comercial' => 'required|unique:medicamentos|max:255',
        ]);

        $medicamento = new Medicamento;
        $medicamento->nombre_comercial=$request->nombre_comercial;
        $medicamento->save();

        return redirect()->route('medicamento.index',['posts'=>Medicamento::orderBy('id')->paginate(5)]);
    }

    public function edit(Medicamento $medicamento){
        return view('medicamentos.editMedicamento',['medicamento'=>$medicamento]);
    }

    public function update( Medicamento $medicamento, Request $request,){
        $request->validate([
            'nombre_comercial' => 'required',
        ]);
        $medicamento->nombre_comercial=$request->nombre_comercial;
        $medicamento->save();
        return redirect()->route('medicamento.index',['posts'=>Medicamento::orderBy('id')->paginate(5)]);
    }

    public function delete(Medicamento $medicamento){
        $medicamento->delete();
        return redirect()->route('medicamento.index',['posts'=>Medicamento::orderBy('id')->paginate(5)]);
    }
}
