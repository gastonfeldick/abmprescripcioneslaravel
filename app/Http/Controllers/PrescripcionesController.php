<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersonaMedicamento;
use App\Models\Persona;
use App\Models\Medicamento;

class PrescripcionesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('Prescripciones.tablePrescripciones',['posts'=>PersonaMedicamento::orderBy('id')->paginate(5)]);
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('Prescripciones.createPrescripciones',['personas'=>Persona::all()],['medicamentos'=>Medicamento::all()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'observaciones' => 'required',
            'paciente' => 'required',
            'medicamento' => 'required',
        ]);

        $prescipcion= new PersonaMedicamento;
        $prescipcion->observaciones=$request->observaciones;
        $prescipcion->persona_id=$request->paciente;
        $prescipcion->medicamento_id=$request->medicamento;
        $prescipcion->save();
        return redirect()->route('prescripciones.index',['posts'=>PersonaMedicamento::orderBy('id')->paginate(5)]);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $prescripcion= PersonaMedicamento::find($id);
        return view('Prescripciones.editPrescripcion',['prescripcion'=>$prescripcion,'medicamentos'=>Medicamento::all(),'personas'=>Persona::all()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $prescripcion= PersonaMedicamento::find($id);
        $prescripcion->observaciones=$request->observaciones;
        $prescripcion->persona_id=$request->paciente;
        $prescripcion->medicamento_id=$request->medicamento;
        $prescripcion->save();
        return  redirect()->route('prescripciones.index',['posts'=>PersonaMedicamento::orderBy('id')->paginate(5)]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $prescripcion= PersonaMedicamento::find($id);
        $prescripcion->delete();
        return  redirect()->route('prescripciones.index',['posts'=>PersonaMedicamento::orderBy('id')->paginate(5)]);
    }

    public function BuscarDni(Request $request){
        $prescripciones=PersonaMedicamento::join('personas','persona_medicamento.persona_id','=','personas.id')->where('personas.dni',$request->dni)->paginate(30);
        return  view('Prescripciones.tablePrescripciones',['posts'=>$prescripciones]);
    }

    public function BuscarMedicamento(Request $request){
        $nombreComercial = $request->input('medicamento');
        $personaMedicamentos = PersonaMedicamento::whereHas('medicamento', function($query) use ($nombreComercial) {
            $query->where('nombre_comercial','LIKE','%'. $nombreComercial .'%');
        })->paginate(100);
        return view('Prescripciones.tablePrescripciones',['posts'=>$personaMedicamentos]);;

    }
}
