<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonaMedicamento extends Model
{
    use HasFactory;

    

    protected $fillable = [
        'observaciones',
        'persona_id',
        'medicamento_id',
    ];
    
    public function persona(){
        return $this->belongsTo(Persona::class);
    }
    public function medicamento(){
        return $this->belongsTo(Medicamento::class);
    }

    protected $table="persona_medicamento"; 

            
}