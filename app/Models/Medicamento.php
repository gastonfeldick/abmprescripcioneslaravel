<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    protected $fillable = [
        'nombre_comercial',
    ];
    protected $table="medicamentos"; 

    public function PersonaMedicamento(){
        return $this->hasMany(PersonaMedicamento::class);
    }

}
