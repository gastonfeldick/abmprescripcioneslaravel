@extends('layouts.app')
@section('pestaña','personas')
@section('titulo','PACIENTES')

@section('content')
    <form action="{{route('persona.edit',$persona)}}" method="POST">
        @csrf
        @method('PATCH')
        <div class="bg-slate-50 font-serif block container mx-auto rounded border border-gray-600 w-80 sm:w-1/2 xl:w-1/3  rounded mt-20 ">
            <div class="">
                <div class="bg-cyan-700 flex justify-center algin-center border-green text-4xl text-white p-2 ">
                    <label> Editar Paciente  </label>
                </div>
                <div class="p-2">
                    <div class="mt-3">
                        <input type="text" id="nombre" name="nombre" class=" px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1" placeholder="Nombre" value="{{$persona->nombre}}" />
                    </div>       
                    <div class="mt-3">
                        
                        <input type="text" id="apellido" name="apellido" class=" px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1" placeholder="Apellido" value="{{$persona->apellido}}" />
                    </div>
                    <div class="mt-3">
                        
                        <input type="text" id="dni" name="dni" class=" px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1" placeholder="DNI" value="{{$persona->dni}}" />
                    </div>
                    <div class="mt-3">
                        
                        <input type="date" id="fecha" name="fecha" class=" px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1" value="{{$persona->fecha_nacimiento}}" />
                    </div>
                </div>
                <div class="flex justify-center align-center">
                    <button type="submit" class="bg-gradient-to-r from-green-400 to-blue-500 shadow-xl text-white font-bold border border-gray-200 hover:from-pink-500 hover:to-yellow-500 focus:outline-none focus:ring px-6 py-2 rounded-full m-2">Guardar</button>
                </div> 
            </div>
        </div>
    </form>

@endsection