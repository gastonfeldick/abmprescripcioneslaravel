@extends('layouts.appPrescripciones')
@section('pestaña','prescripciones')
@section('titulo','PRESCRIPCIONES')

@section('content')

<form action="{{route('prescripciones.update',$prescripcion)}}" method="POST">
    @csrf
    @method('PATCH')
    <div class="bg-slate-50 font-serif block container mx-auto rounded border border-gray-600 w-80 sm:w-1/2 xl:w-1/3  rounded mt-20 ">
        <div class="">
            <div class="bg-cyan-700 flex justify-center algin-center border-green text-4xl text-white p-2 ">
                <label> Nueva Receta </label>
            </div>
            <div class="p-2">

                <div class="mt-3">
                    <label data-te-select-label-ref for="countries" class=" ml-2 text-sm font-medium text-gray-900">Paciente</label>
                    <select data-te-select-init  id="paciente" name="paciente" class="px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1">
                        @foreach ($personas as $persona)
                            <option value="{{$persona->id}}"
                                @if ($persona->id==$prescripcion->persona_id)
                                    selected
                                @endif > {{$persona->nombre}} {{$persona->apellido}}
                            </option>
                        @endforeach
                    </select>
                    
                </div>

                <div class="">
                    <label data-te-select-label-ref for="countries" class=" ml-2 text-sm font-medium text-gray-900">Medicamento</label>
                    <select data-te-select-init  id="medicamento" name="medicamento" class="px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1">
                        @foreach ($medicamentos as $medicamento)
                            <option value="{{$medicamento->id}}" 
                                @if ($medicamento->id==$prescripcion->medicamento_id)
                                    selected
                                @endif>{{$medicamento->nombre_comercial}}
                            </option>
                        @endforeach
                    </select>
                    
                </div>

                <div class="mt-3">
                    <input type="text" id="observaciones" name="observaciones" class=" px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500 focus:ring-sky-500 block w-full rounded-md sm:text-xl focus:ring-1" placeholder="observaciones" value="{{$prescripcion->observaciones}}"/>
                </div>

                

            </div>
            <div class="flex justify-center align-center">
                <button type="submit" class=" bg-gradient-to-r from-green-400 to-blue-500 shadow-xl text-white font-bold border border-gray-200 hover:from-pink-500 hover:to-yellow-500 focus:outline-none focus:ring px-6 py-2 rounded-full m-2">Guardar</button>
            </div> 
            
            
            
        </div>
    </div>
</form>



   

    


    
@endsection