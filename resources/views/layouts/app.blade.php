<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('pestaña')</title>
    <link rel="icon" href="https://www.integrandosalud.com/src-v2/public/bootstrap/img/favicon/favicon.ico" type="image/x-icon">
    @vite('resources/css/app.css')
</head>

<body class="bg-slate-100">

    <nav class="font-serif p-1 bg-gradient-to-r from-blue-800  to-cyan-400 top-0 left-0 border-b border-gray-200 content-center" id="nav">
        <div class="grid grid-cols-3 content-center ">
            <div></div>
            <div class="flex justify-center items-center text-white  text-2xl md:text-5xl "><label id="titulo">@yield('titulo')</label> </div>
            <div class="my-1 flex justify-around md:mx-3"> 
                    <a href="{{route('index')}}" class="text-black font-semibold text-xl hover:bg-cyan-200 px-1 py-1 rounded">Menu</a>
                    <a href="{{route('personas')}}" class="text-black font-semibold text-xl hover:bg-cyan-200 px-1 py-1 rounded">Crear</a>
                    <a href="{{route('tablaPersonas')}}" class="text-black font-semibold text-xl hover:bg-cyan-200 px-1 py-1 rounded">Tabla</a>
            </div>
        </div>
    </nav>

    @yield('content')

    
</body>
</html>