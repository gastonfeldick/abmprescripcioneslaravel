<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonasController;
use App\Http\Controllers\MedicamentosController;
use App\Http\Controllers\PrescripcionesController;


/*
La idea fue experimentar con las rutas de laravel 

comence haciendo las vistas desordenadas 
*/

Route::get('/',[PersonasController::class,'index'])->name('index');
Route::get('/personas',[PersonasController::class,'nuevaPersona'])->name('personas');
Route::post('/',[PersonasController::class,'store'])->name('insertarPersona');
Route::get('/personas-tabla',[PersonasController::class,'tablaPersonas'])->name('tablaPersonas');
Route::get('/edit-personas/{persona}',[PersonasController::class,'edit'])->name('editarPersona');
Route::delete('/eliminar/{persona}',[PersonasController::class,'destroy'])->name('eliminarPersona');
Route::patch('/edit-personas/{persona}',[PersonasController::class,'update'])->name('persona.edit');

/* vistas por convención */
Route::get('/medicamentos/crear',[MedicamentosController::class,'create'])->name('medicamento.create');
Route::get('/medicamentos/tabla',[MedicamentosController::class,'index'])->name('medicamento.index');
Route::post('/medicamento/insert',[MedicamentosController::class,'store'])->name('medicamento.store');
Route::get('/medicamentos/editar/{medicamento}',[MedicamentosController::class,'edit'])->name('medicamento.edit');
Route::patch('/medicamentos/update/{medicamento}',[MedicamentosController::class,'update'])->name('medicamento.update');
Route::delete('/medicamentos/delete/{medicamento}',[MedicamentosController::class,'delete'])->name('medicamento.delete');

/* Vistas producidas por laravel*/
Route::resource('/prescripciones', PrescripcionesController::class);

//adicionales para los filtros
Route::post('prescripciones/buscar-dni',[PrescripcionesController::class,'BuscarDni'])->name('buscarDni');
Route::post('prescripciones/buscar-medicamento',[PrescripcionesController::class,'BuscarMedicamento'])->name('buscarMedicamento');