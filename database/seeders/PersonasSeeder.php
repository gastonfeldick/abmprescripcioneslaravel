<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $personas = [
            ['id' => 1, 'nombre' => 'Gaston', 'apellido' => 'Feldick', 'dni' => '4111340', 'fecha_nacimiento' => '1998-04-23', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 2, 'nombre' => 'Nicolas', 'apellido' => 'Merentiel', 'dni' => '3321333', 'fecha_nacimiento' => '1998-01-24', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 3, 'nombre' => 'Rodrigo', 'apellido' => 'Bertoli', 'dni' => '41333112', 'fecha_nacimiento' => '1998-02-12', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 4, 'nombre' => 'Emanuel', 'apellido' => 'Feldman', 'dni' => '42133131', 'fecha_nacimiento' => '1998-04-12', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 5, 'nombre' => 'Nicolás', 'apellido' => 'Wetemms', 'dni' => '43131441', 'fecha_nacimiento' => '1997-03-12', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
        ];


        foreach($personas as $persona){
            DB::table('personas')->insert($persona);
        }

    }
}
