<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicamentosSeeders extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $Medicamentos = [
            ['id' => 1, 'nombre_comercial' => 'Ibuprofeno', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 2, 'nombre_comercial' => 'Tafirol', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 3, 'nombre_comercial' => 'Omeprazol', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 4, 'nombre_comercial' => 'Ramipiril', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
            ['id' => 5, 'nombre_comercial' => 'Lanzoprazol', 'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),],
        ];

        foreach($Medicamentos as $medicamento){
            DB::table('medicamentos')->insert($medicamento);
        }
    }
}
